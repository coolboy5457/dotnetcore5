using Library.Service.Implement;
using Library.Service.Interface;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics.CodeAnalysis;

namespace TestIntegration
{
    [TestClass]
    public class DiTest 
    {
        [TestMethod]
        public void TestMethod1()
        {
            var diTool = new BaseDiToolFromDotNetCore5();  
            
            var obj = diTool.GetService<IDiService>();
            var result = obj.get();
        }

        [TestMethod]
        public void TestMethod2()
        {
            var diTool = new BaseDiToolFromEmpty();
            var obj = diTool.GetService<IDiService, DiService>();
            var result = obj.get();
        }
    }
}
