using Library.Service.Implement;
using Library.Service.Interface;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics.CodeAnalysis;

namespace TestIntegration
{
    /// <summary>
    /// �Ū�DI�`�J
    /// </summary>
    [TestClass]
    public class BaseDiToolFromEmpty
    {
        IServiceCollection serviceCollection = null;
        IServiceProvider serviceProvider = null;
        IServiceScope scope = null;

        public BaseDiToolFromEmpty() 
        {
            Init();
        }

        protected void Init()
        {
            serviceCollection = new ServiceCollection();
        }

        public TService GetService<TService, [DynamicallyAccessedMembers(DynamicallyAccessedMemberTypes.PublicConstructors)] TImplementation>() 
            where TService : class
            where TImplementation : class, TService
        {
            serviceCollection.AddTransient<TService, TImplementation>();
            serviceProvider = serviceCollection.BuildServiceProvider(validateScopes: true);
            scope = serviceProvider.CreateScope();
           
            var result = scope.ServiceProvider.GetRequiredService<TService>();
            return result;
        }

        [TestMethod]
        public void TestMethod1()
        {
            Init();

            var obj = GetService<IDiService, DiService>();
            var result = obj.get();
        }
    }



}
