using Library.Service.Implement;
using Library.Service.Interface;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestIntegration
{

    /// <summary>
    /// 從現有的 Startup 注入
    /// </summary>
    [TestClass]
    public class BaseDiToolFromDotNetCore5
    {
        IWebHost _webHost = null;

        public BaseDiToolFromDotNetCore5() 
        {
            Init();
        }

        protected void Init()
        {
            _webHost = WebHost.CreateDefaultBuilder()
                .UseStartup<DotNetCore5.Startup>() //未來可以從這邊切換Startup
                .Build();
        }

        public T GetService<T>()
        {
            var scope = _webHost.Services.CreateScope();
            return scope.ServiceProvider.GetRequiredService<T>();
        }

        [TestMethod]
        public void TestMethod1()
        {
            Init();

            var obj = GetService<IDiService>();
            var result = obj.get();
            Assert.IsNotNull(result);
        }
    }
}
