﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetCore5Library
{
    public class IocManager
    {
        private static readonly object padlock = new object();

        private static IocManager _instance = null;

        private IServiceProvider serviceProvider;

        private IocManager()
        {
        }

        public static IocManager Instance
        {
            get
            {
                lock (padlock)
                {
                    if (_instance == null)
                    {
                        _instance = new IocManager();
                    }
                    return _instance;
                }
            }
        }

        public void set(IServiceProvider _serviceProvider)
        {
            this.serviceProvider = _serviceProvider;
        }

        public IServiceProvider get() 
        {
            return this.serviceProvider;
        }

    }
}
