﻿using Library.DIType;
using Library.Service.Interface;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace NetCore5Library
{
    /// <summary>
    /// 處裡自動注入
    /// </summary>
    public class IocFlow
    {
        public static void Load(IServiceCollection services) 
        {
            List<Type> TypesToRegister = Assembly.Load("Library")
                          .GetTypes()
                          .Where(x => !string.IsNullOrEmpty(x.Namespace))
                          .Where(x => x.IsClass)
                          .Where(x => x.Namespace.StartsWith("Library.Service")).ToList();

            var ScopeType = typeof(IDiTypeScope);
            var TypeTransient = typeof(IDiTypeTransient);
            var TypeSingleton = typeof(IDiTypeSingleton);

            foreach (Type item in TypesToRegister)
            {
                var ImpletmentName = item.Name;
                var InterfaceName = "I" + ImpletmentName;
                var Interface = item.GetInterface(InterfaceName);

                if (ScopeType.IsAssignableFrom(item) && ScopeType != item)
                {
                    services.AddScoped(Interface, item);
                }
                else if (TypeTransient.IsAssignableFrom(item) && TypeTransient !=item)
                {
                    services.AddTransient(Interface, item);
                }
                else if (TypeSingleton.IsAssignableFrom(item) && TypeSingleton != item)
                {
                    services.AddSingleton(Interface, item);
                }

            }
        }
    }
}
