﻿using Library.DIType;
using Library.Service.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Service.Implement
{
    public class DiSingletonService : IDiSingletonService, IDiTypeSingleton
    {
        public string get()
        {
            return "Hello World";
        }
    }
}
