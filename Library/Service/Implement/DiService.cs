﻿using Library.DIType;
using Library.Service.Interface;
using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Service.Implement
{
    public class DiService : IDiService, IDiTypeTransient
    {
        public string get()
        {
            return "Hello World";
        }
    }
}
