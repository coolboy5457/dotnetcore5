﻿using Library.DIType;
using Library.Service.Interface;

namespace Library.Service.Implement
{
    public class DiTransientService : IDiTransientService, IDiTypeTransient
    {
        public string get()
        {
            return "Hello World";
        }
    }
}
