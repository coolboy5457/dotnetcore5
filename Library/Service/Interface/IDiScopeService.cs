﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Service.Interface
{
    public interface IDiScopeService
    {
        public string get();
    }
}
