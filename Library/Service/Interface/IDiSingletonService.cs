﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.Service.Interface
{
    public interface IDiSingletonService
    {
        public string get();
    }
}
