﻿using Library.Service.Implement;
using Library.Service.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using NetCore5Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotNetCore5.Controllers
{
    [Route("api/[controller]/[Action]")]
    [ApiController]
    public class DiTestController : ControllerBase
    {
        IDiService diService;
        public DiTestController(IDiService _diService) 
        {
            this.diService = _diService;
        }
        
        //一般DI
        [HttpGet]
        public IActionResult testNormalDi()
        {
          return Ok(this.diService.get());
        }

        /// <summary>
        /// nwe獨立的DI容器，並注入物件
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult testIndenpentDiContainer()
        {
            var services = new ServiceCollection();
            services.AddTransient<IDiService, DiService>();

            ServiceProvider serviceProvider = services.BuildServiceProvider();
            var service = serviceProvider.GetRequiredService<IDiService>();

            return Ok(service.get());
        }

        /// <summary>
        /// nwe獨立的DI容器，並產生命週期，並注入物件
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult testIndenpentDiScop()
        {
            //注入
            var services = new ServiceCollection();
            services.AddTransient<IDiService, DiService>();

            //產一個獨立的注入容器、並試定Scop
            var result = "";
            using ServiceProvider serviceProvider = services.BuildServiceProvider(validateScopes: true);
            using (IServiceScope scope = serviceProvider.CreateScope())
            {
                // Correctly scoped resolution
                var service = scope.ServiceProvider.GetRequiredService<IDiService>();//引用物件
                result = service.get();
            }

            // Not within a scope, becomes a singleton
            //var service2 = serviceProvider.GetRequiredService<IDiService>();
            return Ok(result);
        }

        /// <summary>
        /// 在現有的Socp，產生DI物件
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult testGetServiceByResolveDi()
        {
            //引用用見，從Start的DI容器
            var service = IocManager.Instance.get().GetService<IDiService>();
            return Ok(service.get());
        }
    }
}
