﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace IdentityClient.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class IdentityController : ControllerBase
    {
        [HttpGet("")]
        [Authorize(Roles = "admin")]
        public ActionResult<string> Admin()
        {
            return "Yes, only Admin can accrss this API";
        }

        [HttpGet("")]
        [Authorize(Roles = "user")]
        public ActionResult<string> User()
        {
            return "Yes, user can accrss this API";
        }

        [HttpGet("")]
        [Authorize()]
        public ActionResult<string> GetT()
        {
            return "Hello";
        }

        [HttpGet("")]
        public ActionResult<string> Get()
        {
            return "Hello";
        }
    }
}
